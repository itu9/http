package tool;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import word.Word;

public class Tool {

    public static String read(String file) throws IOException {
        Path path = Paths.get(file);
        return new String(Files.readAllBytes(path));
    }

    public static HashMap<String, String> getData(Word word) {
        HashMap<String, String> res = new HashMap<>();
        while (word != null) {
            res.put(word.getValue(), Tool.withoutSp(word.getResult()));
            word = word.getNext();
        }
        return res;
    }

    public static String withoutSp(String str) {
        int ind = 0;
        String ans = "";
        String[] splt = str.split("\\ ");
        for (int i = 0; i < splt.length; i++) {
            ans += ((ind != 0) ? " " : "") + splt[i];
            ind = ((splt[i].equals("")) ? 0 : 1);
        }
        return ans;
    }

    public static void describe(Word word) {
        while (word != null) {
            System.out.println("pathern : " + word.getValue());
            System.out.println("left : <" + word.getLeft() + ">");
            System.out.println("right : <" + word.getRight() + ">");
            System.out.println();
            word = word.getNext();
        }
    }

    public static void show(Word word) {
        while (word != null) {
            System.out.println("pathern : " + word.getValue());
            System.out.println("value : " + word.getResult());
            System.out.println();
            word = word.getNext();
        }
    }

    public static void supLast(Word word) {
        if (word.getNext() == null) {
            return;
        }
        while (word.getNext().getValue() != null) {
            word = word.getNext();
        }
        word.setNext(null);
    }

    public static boolean match(String string, String regex) {
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(string);
        return matcher.find();
    }
}
