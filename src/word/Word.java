package word;

public class Word {
    Word next;
    String value;
    String right;
    String left;
    String result;

    public String getLeft() {
        return left;
    }

    public boolean isLeft(String t) {
        if (this.getLeft() == null) {
            return false;
        }
        return this.getLeft().equals(t);
    }

    public void setLeft(String left) {
        this.left = left;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Word getNext() {
        return next;
    }

    public void setNext(Word next) {
        this.next = next;
    }

    public String getRight() {
        return right;
    }

    public void setRight(String right) {
        this.right = right;
    }

}
