package traitement;

import java.util.HashMap;

import tool.Tool;
import word.*;

public class Automate {
    String regex = "[a-zA-Z0-9_]";

    /**
     * Reconnaissance des mot par l'identification. Ex : a = b & 5 = 4 => a = 5 & b
     * = 4
     * 
     * @param pathern
     * @param str
     * @return : resultat en forme de tableau de String. 0 -> mot clef, 1 -> valeur.
     *  
     */
    public HashMap<String,String> identify(String pathern, String str) {
        Automate automate = new Automate();
        Word word = automate.generate(pathern);
        automate.identify(word, str);
        Tool.supLast(word);
        HashMap<String,String> liste = Tool.getData(word);
        return liste;
    }

    public void identify(Word word, String str) {
        char[] liste = str.toCharArray();
        String t = "";
        String temp = "";
        int ind = 1;
        for (int i = 0; i < liste.length; i++) {
            t = "" + liste[i];
            if (i == liste.length - 1) {
                temp += t;
                word.setResult(temp);
            }
            if (word.getRight().equals(t) && ind == 1) {
                word.setResult(temp);
                word = word.getNext();
                temp = "";
                ind = 0;
            } else if (ind == 1) {
                temp += t;
            }

            if (word.isLeft(t)) {
                ind = 1;
            }
        }
    }

    public Word nextWord(Word tword, String temp, String t) {
        tword.setNext(new Word());
        tword.setValue(temp);
        tword.setRight(t);
        tword = tword.getNext();
        return tword;
    }

    public Word generate(String str) {
        String temp = "";
        String t = "";
        Word word = new Word();
        int left = 1;
        Word tWord = word;
        int ind = 0;
        char[] liste = str.toCharArray();
        String last = "";
        for (int i = 0; i < liste.length; i++) {
            t = String.copyValueOf(liste, i, 1);
            if (Tool.match(t, regex)) {
                temp += t;
                ind = 0;
                if (left == 0) {
                    tWord.setLeft(last);
                    left = 1;
                }
            } else {
                if (ind == 0) {
                    tWord = nextWord(tWord, temp, t);
                }
                temp = "";
                ind = 1;
                left = 0;
            }
            if (i == liste.length - 1 && temp != "") {
                tWord = nextWord(tWord, temp, "");
            }
            last = t;
        }
        return word;
    }
}
