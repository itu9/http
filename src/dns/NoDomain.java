package dns;

public class NoDomain extends Exception {

    public NoDomain() {
    }

    public NoDomain(String domain) {
        super(String.format("%s is not register on this dns", domain));
    }
    
}
