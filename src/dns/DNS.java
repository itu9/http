package dns;

import server.Url;
import server.http.Request;

public class DNS {
    Url[] urls;

    public DNS(Url[] urls) {
        this.setUrls(urls);
    }

    public Url getIp(Request request) throws NoDomain {
        Url url = this.getIp(request.getHeader().getDName(),request.getHeader().getPort()); 
        request.getHeader().setUrl(url);
        return url;
    }

    public Url getIp(String domain, int port) throws NoDomain {
        domain = domain.toLowerCase();
        for (int i = 0; i < this.getUrls().length; i++) {
            String temp = this.getUrls()[i].getDomain().toLowerCase();
            int p = this.getUrls()[i].getPort();
            if (temp.compareTo(domain) == 0 && p == port) {
                return this.getUrls()[i];
            }
        }
        throw new NoDomain(domain);
    }

    public Url[] getUrls() {
        return urls;
    }

    public void setUrls(Url[] urls) {
        this.urls = urls;
    }

}
