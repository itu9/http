package main;

import dns.DNS;
import dns.NoDomain;
import navigateur.Navigateur;
import server.Content;
import server.Server;
import server.Site;
import server.Url;
import server.http.InvalidState;
import server.http.Request;
import server.http.Response;
import server.http.header.Encoding;
import server.http.header.HMethod;
import server.http.header.HType;
import server.http.header.Language;

public class Main {

    public static Site getFacebookSite() {

        Url url = new Url("facebook", "192.168.35.1", 80);
        Content[] contents = {
                new Content("publication",
                        "[{\"id\":1,\"user\":{\"id\":1,\"nom\":\"Hasina\"},\"description\":\"petit description\"}]",
                        HType.JSON),
                new Content("login",
                        "{\"id\":1,\"nom\":\"Hasina\"}",
                        HType.JSON),
        };
        Site site = new Site(url, contents);
        return site;
    }

    //
    public static Site getYouTubeSite() {
        Url url = new Url("youtube", "192.168.37.12", 93);
        Content[] contents = {
                new Content("search", "<h2>Answer</h2>", HType.HTML),
                new Content("videoDesc", "Une petite description", HType.TEXT),
        };
        Site site = new Site(url, contents);
        return site;
    }

    public static Site getYahooSite() {
        Url url = new Url("yahoo", "192.168.50.42", 3000);
        Content[] contents = {
                new Content("data", "[{\"valuer\" : \"La valeur\"}]", HType.JSON)
        };
        Site site = new Site(url, contents);
        return site;
    }

    public static Site getGoogleSite() {
        Url url = new Url("google", "172.13.20.3", 6000);
        Content[] contents = {
                new Content("wikipedia", "un site qui englobe presque la connaissance du monde entier", HType.TEXT,Encoding.UTF),
                new Content("youtube", "un site de videos", HType.TEXT)
        };
        Site site = new Site(url, contents);
        return site;
    }

    public static Site[] gSitesEx() {
        Site[] ans = {
                Main.getFacebookSite(),
                Main.getYouTubeSite(),
                Main.getYahooSite(),
                Main.getGoogleSite()
        };
        return ans;
    }

    public static Url[] getUrlEx(Site[] sites) {
        Url[] urls = new Url[sites.length];
        for (int i = 0; i < urls.length; i++) {
            urls[i] = sites[i].getUrl();
        }
        return urls;
    }

    public static DNS[] getDnsEx(Site[] sites) {
        DNS[] dns = {
                new DNS(getUrlEx(sites)),
        };
        return dns;
    }

    public static Server gServerEx(Site[] sites) {
        Server server = new Server(sites);
        return server;
    }

    public static void main(String[] args) {
        String url = "http://www.google.fr:6000/index?name=Hasina";
        Site[] sites = Main.gSitesEx();
        Server server = new Server(sites);
        Navigateur navigateur = new Navigateur(getDnsEx(sites));
        navigateur.setUrl(url);
        navigateur.setMethod(HMethod.GET);
        navigateur.setLanguage(Language.EN);
        navigateur.setContentType(HType.TEXT);
        navigateur.setEncoding(Encoding.Gzip);
        navigateur.setContent("Hello world");
        Request req = navigateur.gRequest();
        try {
            navigateur.getUrlMatched();
            Response response = server.bind(req);
            System.out.println(response);
        } catch (NoDomain e) {
            e.printStackTrace();
        } catch (InvalidState e) {
            e.printStackTrace();
        }
    }
}
