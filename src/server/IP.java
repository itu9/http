package server;

public class IP {
    int first;
    int second;
    int third;
    int fourth;

    public IP(String ip) {
        this(IP.splt(ip));
    }

    public IP(int... ip) {
        this(ip[0], ip[1], ip[2], ip[3]);
    }

    public IP(int first, int second, int third, int fourth) {
        this.setFirst(first);
        this.setSecond(second);
        this.setThird(third);
        this.setFourth(fourth);
    }

    @Override
    public String toString() {
        return this.concat();
    }

    public static int[] splt(String ip) {
        String[] splt = ip.split("\\.");
        int[] ans = new int[splt.length];
        for (int i = 0; i < ans.length; i++) {
            ans[i] = Integer.parseInt(splt[i]);
        }
        return ans;
    }

    public int sum() {
        return this.getFirst() + this.getSecond() + this.getThird() + this.getFourth();
    }

    public String concat() {
        String ans = "%s.%s.%s.%s";
        ans = String.format(ans, this.getFirst(), this.getSecond(), this.getThird(), this.getFourth());
        return ans;
    }

    /*********************************************************************************************
     *********************************************************************************************
     *********************************************************************************************
     *********************************************************************************************
     * Fin traitement
     *********************************************************************************************
     *********************************************************************************************
     *********************************************************************************************
     *********************************************************************************************
     */

    public int getFirst() {
        return first;
    }

    public void setFirst(int first) {
        this.first = first;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public int getThird() {
        return third;
    }

    public void setThird(int third) {
        this.third = third;
    }

    public int getFourth() {
        return fourth;
    }

    public void setFourth(int fourth) {
        this.fourth = fourth;
    }

}
