package server;

public class NoSite extends Exception {

    public NoSite() {
    }

    public NoSite(String message) {
        super(message);
    }
    
}
