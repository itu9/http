package server;

public class Url {
    String domain;
    IP ip;
    int port;

    public Url(String domain, String ip, int port) {
        this(domain, ip);
        this.setPort(port);
    }

    public Url(String domain, String ip) {
        this.setDomain(domain);
        this.setIp(ip);
    }

    public Url() {
    }

    public int value() {
        return this.getIp().sum() + this.getPort();
    }

    @Override
    public String toString() {
        return "Url [domain=" + domain + ", ip=" + ip + ", port=" + port + "]";
    }

    public boolean compareIP(String ip) {
        return this.getIp().concat().compareTo(ip) == 0;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public IP getIp() {
        return ip;
    }

    public String concat() {
        String pattern = "%s:%s";
        return String.format(pattern, this.getIp().concat(), this.getPort());
    }

    public void setIp(String ip) {
        this.ip = new IP(ip);
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

}
