package server;

import server.http.Body;

public class BodyWork extends Exception {
    Body body;
    Content content;

    public BodyWork(Body body, Content content) {
        this.setBody(body);
        this.setContent(content);
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

}
