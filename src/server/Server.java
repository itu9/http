package server;

import java.time.LocalDateTime;
import java.util.HashMap;

import server.http.Body;
import server.http.InvalidState;
import server.http.Request;
import server.http.Response;
import server.http.State;
import server.http.header.Encoding;
import server.http.header.HType;
import server.http.header.HeaderResponse;

public class Server {
    Site site;
    HashMap<String, Site> siteMapping = new HashMap<>();

    public Server(Site[] site) {
        this.init(site);
    }

    public void init(Site[] site) {
        this.initMap(site);
        this.initBST(site);
    }

    public void initMap(Site[] site) {
        for (int i = 0; i < site.length; i++) {
            System.out.println(site[i].getUrl().concat());
            this.getSiteMapping().put(site[i].getUrl().concat(), site[i]);
        }
    }

    public void initBST(Site[] site) {
        this.setSite(site[0]);
        for (int i = 1; i < site.length; i++) {
            this.getSite().addSite(site[i]);
        }
    }

    public Site find(Url url) throws NoSiteCorresp {
        return this.getSiteMapping().get(url.concat());
    }

    public Response onError(Request req, ServerError error) throws InvalidState {
        State state = new State(502);
        Body body = null;
        try {
            body = new Body(error.toHtml(state));
        } catch (ServerError e) {
            return this.onError(req, e);
        }
        LocalDateTime date = LocalDateTime.now();
        HeaderResponse header = new HeaderResponse(req.getHeader().getLanguage(), HType.HTML, Encoding.UTF, state,
                date);
        Response response = new Response(body, header);
        return response;
    }

    public Response bind(Request request) throws InvalidState {
        try {
            Site tsite = this.find(request.getHeader().getUrl());
            return tsite.answer(request);
        } catch (NoSiteCorresp e) {
            return this.onError(request, e);
        } catch (InvalidState e) {
            return this.onError(request, e);
        } catch (NotContent e) {
            return this.onError(request, e);
        }
    }

    public Site getSite() {
        return site;
    }

    public void setSite(Site site) {
        this.site = site;
    }

    public HashMap<String, Site> getSiteMapping() {
        return siteMapping;
    }

    public void setSiteMapping(HashMap<String, Site> siteMapping) {
        this.siteMapping = siteMapping;
    }

}
