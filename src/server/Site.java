package server;

import java.time.LocalDateTime;

import server.http.Body;
import server.http.InvalidState;
import server.http.Request;
import server.http.Response;
import server.http.State;
import server.http.header.HeaderResponse;

public class Site {
    Url url;
    Content[] content;
    Site left;
    Site right;

    public Site(Url url, Content[] content) {
        this.setUrl(url);
        this.setContent(content);
    }

    public Response answer(Request req) throws InvalidState, NotContent {
        Body body = null;
        State state = new State(200);
        Content content = null;
        try {
            this.gBody(req);
        } catch (NotContent e) {
            e.genereState();
            e.setUrl(req.getHeader().getUrl());
            throw e;
        } catch (BodyWork e) {
            body = e.getBody();
            content = e.getContent();
        }
        HeaderResponse response = this.gHeaderResponse(req, content, state);
        Response res = new Response(body, response);
        return res;
    }

    public HeaderResponse gHeaderResponse(Request req, Content content, State state) {
        LocalDateTime date = LocalDateTime.now();
        HeaderResponse response = new HeaderResponse(content, req, state, date);
        return response;
    }

    public void gBody(Request req) throws NotContent, BodyWork {
        Content content = this.getContent(req.getHeader().getPath());
        Body body = new Body(content.getValue());
        BodyWork ans = new BodyWork(body, content);
        throw ans;
    }

    public Content getContent(String path) throws NotContent {
        System.out.println("Site => path :" + path);
        for (int i = 0; i < this.getContent().length; i++) {
            if (this.getContent()[i].matchPath(path)) {
                return this.getContent()[i];
            }
        }
        throw new NotContent();
    }

    @Override
    public String toString() {
        return "Site [url=" + url + "]";
    }

    public Site findLeft(Url url) throws NoSiteCorresp {
        if (!this.canMoveLeft()) {
            throw new NoSiteCorresp(url.getIp().concat(), url);
        }
        return this.getLeft().find(url);
    }

    public Site findRight(Url url) throws NoSiteCorresp {
        if (!this.canMoveRight()) {
            throw new NoSiteCorresp(url.getIp().concat(), url);
        }
        return this.getRight().find(url);
    }

    public Site find(Url url) throws NoSiteCorresp {
        if (this.equals(url)) {
            return this;
        }
        if (url.value() <= this.value()) {
            return this.findLeft(url);
        } else {
            return this.findRight(url);
        }
    }

    public boolean equals(Url obj) {
        return this.getUrl().compareIP(obj.getIp().concat())
                && this.getUrl().getPort() == obj.getPort();
    }

    public void addLeft(Site site) {
        if (this.canMoveLeft()) {
            this.getLeft().addSite(site);
        } else {
            this.setLeft(site);
        }
    }

    public void addRight(Site site) {
        if (this.canMoveRight()) {
            this.getRight().addSite(site);
        } else {
            this.setRight(site);
        }
    }

    public void addSite(Site site) {
        if (site.value() <= this.value()) {
            this.addLeft(site);
        } else {
            this.addRight(site);
        }
    }

    public int value() {
        return this.getUrl().value();
    }

    public boolean canMoveLeft() {
        return this.getLeft() != null;
    }

    public boolean canMoveRight() {
        return this.getRight() != null;
    }

    public boolean isLeaf() {
        boolean verif = true && this.getLeft() == null;
        verif &= this.getRight() == null;
        return verif;
    }

    /*********************************************************************************************
     *********************************************************************************************
     *********************************************************************************************
     *********************************************************************************************
     * Fin traitement
     *********************************************************************************************
     *********************************************************************************************
     *********************************************************************************************
     *********************************************************************************************
     */
    public Url getUrl() {
        return url;
    }

    public void setUrl(Url url) {
        this.url = url;
    }

    public Content[] getContent() {
        return content;
    }

    public void setContent(Content[] content) {
        this.content = content;
    }

    public Site getLeft() {
        return left;
    }

    public void setLeft(Site left) {
        this.left = left;
    }

    public Site getRight() {
        return right;
    }

    public void setRight(Site right) {
        this.right = right;
    }

}
