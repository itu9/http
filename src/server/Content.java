package server;

import server.http.header.Encoding;
import server.http.header.HType;

public class Content {
    String path;
    String value;
    HType contentType;
    Encoding encoding;

    public Content(String path, String value, HType contentType, Encoding encoding) {
        this(path, value, contentType);
        this.setEncoding(encoding);
    }

    public Content(String path, String value, HType contentType) {
        this.setPath(path);
        this.setValue(value);
        this.setContentType(contentType);
    }

    public boolean matchPath(String path) {
        return this.getPath().compareTo(path) == 0;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public HType getContentType() {
        return contentType;
    }

    public void setContentType(HType contentType) {
        this.contentType = contentType;
    }

    public Encoding getEncoding() {
        return encoding;
    }

    public void setEncoding(Encoding encoding) {
        this.encoding = encoding;
    }

}
