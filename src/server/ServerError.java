package server;

import java.io.IOException;

import server.http.State;
import tool.Tool;

public class ServerError extends Exception {

    public ServerError() {
    }

    public ServerError(String message) {
        super(message);
    }

    public String toHtml(State state) throws ServerError {
        String pattern = null;
        try {
            pattern = Tool.read("lib/error.html");
        } catch (IOException e) {
            throw new ServerError();
        }
        return String.format(pattern, state.toString(), this.getMessage());
    }

}
