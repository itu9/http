package server;

public class NoSiteCorresp extends ServerError {
    Url url;

    public NoSiteCorresp() {
    }

    public NoSiteCorresp(String ip, Url url) {
        super(String.format("No site match to the ip : %s", ip));
        this.setUrl(url);
    }

    public Url getUrl() {
        return url;
    }

    public void setUrl(Url url) {
        this.url = url;
    }

}
