package server.http;

import server.http.header.HeaderResponse;

public class Response extends Http {
    HeaderResponse header;

    public Response(Body body, HeaderResponse headerResponse) {
        super(body);
        this.setHeader(headerResponse);
    }

    @Override
    public String toString() {
        return "Response [header=" + header.toString() + "," + this.getBody().toString() + "]";
    }

    public HeaderResponse getHeader() {
        return header;
    }

    public void setHeader(HeaderResponse header) {
        this.header = header;
    }

}
