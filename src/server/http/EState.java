package server.http;

/**
 * {@link EState#S1XX S1XX} : Informational
 * {@link EState#S2XX S2XX} : Success
 * {@link EState#S3XX S3XX} : Redirection
 * {@link EState#S4XX S4XX} : Client Error
 * {@link EState#S5XX S5XX} : Server Error
 */
public enum EState {
    S1XX(100, 199, "Informational"), S2XX(200, 299, "Success"), S3XX(300, 399, "Redirection"),
    S4XX(400, 499, "Client Error"), S5XX(500, 599, "Server Error");

    int start;
    int end;
    String message;

    private EState(int start, int end, String message) {
        this.setStart(start);
        this.setEnd(end);
        this.setMessage(message);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static EState getState(int value) throws InvalidState {
        EState[] eStates = EState.values();
        for (int i = 0; i < eStates.length; i++) {
            if (eStates[i].getStart() <= value && eStates[i].getEnd() >= value) {
                return eStates[i];
            }
        }
        throw new InvalidState();
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

}
