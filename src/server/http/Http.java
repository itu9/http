package server.http;

public class Http {
    Body body;

    public Http(Body body) {
        this.setBody(body);
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

}
