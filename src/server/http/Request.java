package server.http;

import server.http.header.HeaderRequest;

public class Request extends Http {
    HeaderRequest header;

    public Request(Body body, HeaderRequest header) {
        super(body);
        this.setHeader(header);
    }

    public HeaderRequest getHeader() {
        return header;
    }

    public void setHeader(HeaderRequest header) {
        this.header = header;
    }

}
