package server.http;

public class State {
    EState eState;
    int code;

    public State(int code) throws InvalidState {
        this.setCode(code);
    }

    @Override
    public String toString() {
        String s = "%s code : %s";
        return String.format(s, this.geteState().getMessage(), this.getCode());
    }

    public EState geteState() {
        return eState;
    }

    public void seteState(EState eState) {
        this.eState = eState;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) throws InvalidState {
        this.code = code;
        this.seteState(EState.getState(code));
    }

}
