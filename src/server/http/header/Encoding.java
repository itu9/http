package server.http.header;

public enum Encoding {
    Gzip("gzip"),UTF("UTF-8");

    String label;

    private Encoding(String label) {
        this.setLabel(label);
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }


}
