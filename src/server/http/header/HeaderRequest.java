package server.http.header;

import java.util.HashMap;

import navigateur.Navigateur;
import server.Url;
import traitement.Automate;

public class HeaderRequest extends Header {
    public static String pdomain = "key.domain.suf";
    HMethod method;
    Url url = new Url();
    String path;
    HashMap<String, String> domMapping;

    public HeaderRequest(Language language, HType contentType, Encoding encoding, HMethod method, String authority,
            String path, int port) {
        super(language, contentType, encoding);
        this.setMethod(method);
        this.setAuthority(authority);
        this.setPort(port);
        this.setPath(path);
        this.init();
    }

    @Override
    public String toString() {
        return "HeaderRequest [method=" + method + ", path=" + path + ", " + super.toString() + "]";
    }

    public HeaderRequest(Navigateur navigateur) {
        this(navigateur.getLanguage(), navigateur.getContentType(), navigateur.getEncoding(), navigateur.getMethod(),
                navigateur.getDomain(), navigateur.getPath(),navigateur.getPort());
    }

    public void init() {
        Automate automate = new Automate();
        this.setDomMapping(automate.identify(HeaderRequest.pdomain, this.getAuthority()));
    }

    public String getDName() {
        return this.getDValue("domain");
    }

    public String getDValue(String key) {
        return this.getDomMapping().get(key);
    }

    /*********************************************************************************************
     *********************************************************************************************
     *********************************************************************************************
     *********************************************************************************************
     * Fin traitement
     *********************************************************************************************
     *********************************************************************************************
     *********************************************************************************************
     *********************************************************************************************
     */

    public int getPort() {
        return this.getUrl().getPort();
    }

    public void setPort(int port) {
        this.getUrl().setPort(port);
    }

    public HashMap<String, String> getDomMapping() {
        return domMapping;
    }

    public void setDomMapping(HashMap<String, String> domMapping) {
        this.domMapping = domMapping;
    }

    public HMethod getMethod() {
        return method;
    }

    public void setMethod(HMethod method) {
        this.method = method;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getAuthority() {
        return this.getUrl().getDomain();
    }

    public void setAuthority(String authority) {
        this.getUrl().setDomain(authority);
    }

    public static String getPdomain() {
        return pdomain;
    }

    public static void setPdomain(String pdomain) {
        HeaderRequest.pdomain = pdomain;
    }

    public Url getUrl() {
        return url;
    }

    public void setUrl(Url url) {
        this.url = url;
    }

}
