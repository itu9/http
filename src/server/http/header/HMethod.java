package server.http.header;

public enum HMethod {
    GET, POST, PUT, DELETE;
}
