package server.http.header;

public class Header {
    Language language;
    HType contentType;
    Encoding encoding;

    public Header(Language language, HType contentType, Encoding encoding) {
        this.setLanguage(language);
        this.setContentType(contentType);
        this.setEncoding(encoding);
    }

    @Override
    public String toString() {
        return "Header [language=" + language + ", contentType=" + contentType + ", encoding=" + encoding + "]";
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public HType getContentType() {
        return contentType;
    }

    public void setContentType(HType contentType) {
        this.contentType = contentType;
    }

    public Encoding getEncoding() {
        return encoding;
    }

    public void setEncoding(Encoding encoding) {
        this.encoding = encoding;
    }

}
