package server.http.header;

public enum Language {
    EN("en-US"),FR("fr");
    String label;

    private Language(String label) {
        this.setLabel(label);
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }


}
