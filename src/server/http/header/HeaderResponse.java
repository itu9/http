package server.http.header;

import java.time.LocalDateTime;

import server.Content;
import server.http.Request;
import server.http.State;

public class HeaderResponse extends Header {

    State state;
    LocalDateTime date;

    public HeaderResponse(Language language, HType contentType, Encoding encoding, State state, LocalDateTime date) {
        super(language, contentType, encoding);
        this.setState(state);
        this.setDate(date);
    }

    public HeaderResponse(Content content, Request request, State state, LocalDateTime date) {
        super(request.getHeader().getLanguage(), content.getContentType(), content.getEncoding());
        this.state = state;
        this.date = date;
    }

    @Override
    public String toString() {
        return "HeaderResponse [state=" + state + ", date=" + date + "," + super.toString() + "]";
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

}
