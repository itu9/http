package server.http.header;

public enum HType {
    HTML("text/html"), TEXT("text/plain"), JSON("application/json");

    String label;

    private HType(String label) {
        this.setLabel(label);
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

}
