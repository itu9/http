package server.http;

public class Body {
    String content;

    @Override
    public String toString() {
        return "Body [content=" + content + "]";
    }

    public Body(String content) {
        this.setContent(content);
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
