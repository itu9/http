package server;

import server.http.InvalidState;
import server.http.State;

public class NotContent extends ServerError {
    int code;
    Url url;
    State state;
    String mes;

    public NotContent() {
        this(502);
    }

    public NotContent(String message) {
        super(message);
    }

    public NotContent(int code) {
        this.setCode(code);
    }

    public void genereState() throws InvalidState {
        this.setState(new State(this.getCode()));
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Url getUrl() {
        return url;
    }

    public void setUrl(Url url) {
        this.url = url;
        String message = "%s is undefined for this site.";
        this.setMes(String.format(message, this.getUrl().concat()));
    }


    @Override
    public String getMessage() {
        System.out.println(this.mes);
        return this.getMes();
    }

    public void setMes(String message) {
        this.mes = message;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public String getMes() {
        return mes;
    }

}
