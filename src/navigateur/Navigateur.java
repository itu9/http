package navigateur;

import java.util.HashMap;

import dns.DNS;
import dns.NoDomain;
import server.Url;
import server.http.Body;
import server.http.Request;
import server.http.header.Encoding;
import server.http.header.HMethod;
import server.http.header.HType;
import server.http.header.HeaderRequest;
import server.http.header.Language;
import traitement.Automate;

public class Navigateur {
    public static String pattern = "key://domain:port/path?data";
    String content;
    String url;
    Language language;
    HType contentType;
    Encoding encoding;
    HMethod method;
    Request request;
    DNS[] dns;
    HashMap<String, String> urlMapping;

    public Navigateur(DNS[] dns) {
        this.setDns(dns);
    }

    public Url getUrlMatched() throws NoDomain {
        for (int i = 0; i < this.getDns().length; i++) {
            try {
                return this.getDns()[i].getIp(this.getRequest());
            } catch (NoDomain e) {

            }
        }
        throw new NoDomain(this.getRequest().getHeader().getDName());
    }

    public Body gBody() {
        Body body = new Body(this.getContent());
        return body;
    }

    public HeaderRequest gHeaderRequest() {
        HeaderRequest header = new HeaderRequest(this);
        return header;
    }

    public Request gRequest() {
        this.filter(this.getUrl());
        Body body = this.gBody();
        HeaderRequest header = this.gHeaderRequest();
        Request req = new Request(body, header);
        this.setRequest(req);
        return req;
    }

    public Request gRequest(String url, String content) {
        this.setContent(content);
        this.setUrl(url);
        return this.gRequest();
    }

    public void filter() {
        Automate automate = new Automate();
        this.setUrlMapping(automate.identify(Navigateur.pattern, this.getUrl()));
    }

    public void filter(String url) {
        this.setUrl(url);
        this.filter();
    }

    public String getData() {
        return this.getUrlValue("data");
    }

    public String getPath() {
        return this.getUrlValue("path");
    }

    public int getPort() {
        return Integer.parseInt(this.getUrlValue("port"));
    }

    public String getDomain() {
        return this.getUrlValue("domain");
    }

    public String getKey() {
        return this.getUrlValue("key");
    }

    public String getUrlValue(String key) {
        return this.getUrlMapping().get(key);
    }

    /*********************************************************************************************
     *********************************************************************************************
     *********************************************************************************************
     *********************************************************************************************
     * Fin traitement
     *********************************************************************************************
     *********************************************************************************************
     *********************************************************************************************
     *********************************************************************************************
     */
    public DNS[] getDns() {
        return dns;
    }

    public void setDns(DNS[] dns) {
        this.dns = dns;
    }

    public static String getPattern() {
        return pattern;
    }

    public static void setPattern(String pattern) {
        Navigateur.pattern = pattern;
    }

    public HashMap<String, String> getUrlMapping() {
        return urlMapping;
    }

    public void setUrlMapping(HashMap<String, String> urlMapping) {
        this.urlMapping = urlMapping;
    }

 
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public HType getContentType() {
        return contentType;
    }

    public void setContentType(HType contentType) {
        this.contentType = contentType;
    }

    public Encoding getEncoding() {
        return encoding;
    }

    public void setEncoding(Encoding encoding) {
        this.encoding = encoding;
    }

    public HMethod getMethod() {
        return method;
    }

    public void setMethod(HMethod method) {
        this.method = method;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

}
