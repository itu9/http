package navigateur;

public enum KeyWord {
    Protocol("http", "https", "ftp");

    String[] key;

    private KeyWord(String... key) {
        this.setKey(key);
    }

    public String[] getKey() {
        return key;
    }

    private void setKey(String[] key) {
        this.key = key;
    }

}
